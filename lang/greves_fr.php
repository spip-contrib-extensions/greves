<?php
/** Fichier de langue de SPIP **/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'action_ajouter'		=>'Programmer une gr&egrave;ve',
	'explications'			=>'Entre les dates comprises dans une grève, on ne peut plus consulter le site. À la place s\'affiche le texte de la grève',
	'greves'				=> 'Gr&egrave;ves',
	'info_modifier_greve'	=>'Modifier une gr&egrave;ve',
	'label_debut'			=>'D&eacute;but [Obligatoire]',
	'label_id'				=> 'Id',
	'label_fin'				=>'Fin [Obligatoire]',
	'label_titre'			=>'Titre',
	'label_texte'			=>'Texte',
	'liste_des_greves'		=>'Liste des grèves',
	'texte_nouvelle_greve'	=>'Nouvelle gr&egrave;ve'

);

?>