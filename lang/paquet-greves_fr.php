<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

///  Fichier produit par PlugOnet
// Module: paquet-greves
// Langue: fr
// Date: 16-01-2012 15:35:37
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// G
	'greves_description' => 'Permet de mettre temporairement un site hors service pour protester. On peut éditer une grève, qui reçoit un titre, un texte explicatif, une date de début et une de fin.
    
    Entre la date de début et la date de fin, le site n\'affiche plus que le titre de la grève et son explication.',
	'greves_slogan' => 'Ce n\'est qu\'un début, continuons le combat !',
);
?>